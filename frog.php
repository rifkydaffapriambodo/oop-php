<?php
require_once 'animal.php';
Class Frog extends Animal{
    public function __construct($name){
        parent::set_name($name);
        parent::set_legs(4);
        parent::set_cold_blooded(false);
    }
    public function jump(){
        echo "hop hop";
    }
}
?>
