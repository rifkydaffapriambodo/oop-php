<?php
require_once 'animal.php';
Class Ape extends Animal{
    public function __construct($name){
        parent::set_name($name);
        parent::set_legs(2);
        parent::set_cold_blooded(false);
        
    }
    public function yell(){
        echo "Auooo";
    }
}
?>
