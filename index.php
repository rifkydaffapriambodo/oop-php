<?php

require_once 'animal.php';
$sheep = new Animal("shaun");

echo $sheep->get_name(). "<br>"; // "shaun"
echo $sheep->get_legs(). "<br>"; // 2
var_dump($sheep->get_cold_blooded()); // false
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

require_once 'ape.php';
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";
require_once 'frog.php';
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
?>